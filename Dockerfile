FROM registry.gitlab.com/xuyuhang-docker/alpine:3.14.1
WORKDIR /root

RUN apk add --update git bash openssh-client curl \
    rm -rf /var/cache/apk/*

ENV DOCKER_VERSION 20.10.8
RUN curl -L -o /tmp/docker-${DOCKER_VERSION}.tgz https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz \
    && tar -xz -C /tmp -f /tmp/docker-${DOCKER_VERSION}.tgz \
    && mv /tmp/docker/docker /usr/bin \
    && rm -rf /tmp/docker-${DOCKER_VERSION} /tmp/docker
